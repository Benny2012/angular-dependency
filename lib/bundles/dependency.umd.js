(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/core')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/common', '@angular/core'], factory) :
	(factory((global.ng = global.ng || {}, global.ng.dependency = global.ng.dependency || {}),global.ng.common,global.ng.core));
}(this, (function (exports,_angular_common,_angular_core) { 'use strict';

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent.decorators = [
        { type: _angular_core.Component, args: [{
                    selector: 'app-dependency',
                    templateUrl: './app.component.html',
                    styleUrls: ['./app.component.css']
                },] },
    ];
    /** @nocollapse */
    AppComponent.ctorParameters = function () { return []; };
    return AppComponent;
}());

var AppModule = (function () {
    function AppModule() {
    }
    AppModule.decorators = [
        { type: _angular_core.NgModule, args: [{
                    declarations: [
                        AppComponent
                    ],
                    imports: [
                        _angular_common.CommonModule
                    ],
                    providers: [],
                    exports: [
                        AppComponent
                    ],
                    bootstrap: [AppComponent]
                },] },
    ];
    /** @nocollapse */
    AppModule.ctorParameters = function () { return []; };
    return AppModule;
}());

/**
 * So you’ll need to create an index.ts file, exporting all the public API of your module. It should at least
 * contain your NgModule, and your components or services (the user will need to import them to inject
 * them where they are needed).
 *
 * Source: https://medium.com/@cyrilletuzi/how-to-build-and-publish-an-angular-module-7ad19c0b4464
 */

// services
// pipes
// classes

exports.AppModule = AppModule;
exports.AppComponent = AppComponent;

Object.defineProperty(exports, '__esModule', { value: true });

})));
