/**
 * So you’ll need to create an index.ts file, exporting all the public API of your module. It should at least
 * contain your NgModule, and your components or services (the user will need to import them to inject
 * them where they are needed).
 *
 * Source: https://medium.com/@cyrilletuzi/how-to-build-and-publish-an-angular-module-7ad19c0b4464
 */
export { AppModule } from './app/app.module';
/**
 * Components/directives/pipes won’t be imported directly by the user, but you need to export them to be AoT compatible
 *
 * Source: https://medium.com/@cyrilletuzi/how-to-build-and-publish-an-angular-module-7ad19c0b4464
 */
// components
export { AppComponent } from './app/app.component';
// services
// pipes
// classes
//# sourceMappingURL=index.js.map