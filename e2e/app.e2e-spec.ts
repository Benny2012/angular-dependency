import { DependencyPage } from './app.po';

describe('dependency App', () => {
  let page: DependencyPage;

  beforeEach(() => {
    page = new DependencyPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
