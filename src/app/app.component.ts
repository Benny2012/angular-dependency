import { Component } from '@angular/core';

@Component({
  selector: 'app-dependency',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
